/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.facebook.ads;

public final class R {
    private R() {}

    public static final class attr {
        private attr() {}

        public static final int adSize = 0x7f010000;
        public static final int adSizes = 0x7f010001;
        public static final int adUnitId = 0x7f010002;
        public static final int buttonSize = 0x7f010003;
        public static final int circleCrop = 0x7f010004;
        public static final int colorScheme = 0x7f010005;
        public static final int imageAspectRatio = 0x7f01000f;
        public static final int imageAspectRatioAdjust = 0x7f010010;
        public static final int layoutManager = 0x7f010011;
        public static final int reverseLayout = 0x7f010012;
        public static final int scopeUris = 0x7f010013;
        public static final int spanCount = 0x7f010014;
        public static final int stackFromEnd = 0x7f010015;
    }
    public static final class color {
        private color() {}

        public static final int common_google_signin_btn_text_dark = 0x7f030000;
        public static final int common_google_signin_btn_text_dark_default = 0x7f030001;
        public static final int common_google_signin_btn_text_dark_disabled = 0x7f030002;
        public static final int common_google_signin_btn_text_dark_focused = 0x7f030003;
        public static final int common_google_signin_btn_text_dark_pressed = 0x7f030004;
        public static final int common_google_signin_btn_text_light = 0x7f030005;
        public static final int common_google_signin_btn_text_light_default = 0x7f030006;
        public static final int common_google_signin_btn_text_light_disabled = 0x7f030007;
        public static final int common_google_signin_btn_text_light_focused = 0x7f030008;
        public static final int common_google_signin_btn_text_light_pressed = 0x7f030009;
    }
    public static final class dimen {
        private dimen() {}

        public static final int item_touch_helper_max_drag_scroll_per_frame = 0x7f040005;
        public static final int item_touch_helper_swipe_escape_max_velocity = 0x7f040006;
        public static final int item_touch_helper_swipe_escape_velocity = 0x7f040007;
    }
    public static final class drawable {
        private drawable() {}

        public static final int common_full_open_on_phone = 0x7f050000;
        public static final int common_google_signin_btn_icon_dark = 0x7f050001;
        public static final int common_google_signin_btn_icon_dark_focused = 0x7f050002;
        public static final int common_google_signin_btn_icon_dark_normal = 0x7f050003;
        public static final int common_google_signin_btn_icon_light = 0x7f050006;
        public static final int common_google_signin_btn_icon_light_focused = 0x7f050007;
        public static final int common_google_signin_btn_icon_light_normal = 0x7f050008;
        public static final int common_google_signin_btn_text_dark = 0x7f05000a;
        public static final int common_google_signin_btn_text_dark_focused = 0x7f05000b;
        public static final int common_google_signin_btn_text_dark_normal = 0x7f05000c;
        public static final int common_google_signin_btn_text_light = 0x7f05000f;
        public static final int common_google_signin_btn_text_light_focused = 0x7f050010;
        public static final int common_google_signin_btn_text_light_normal = 0x7f050011;
        public static final int ic_plusone_medium_off_client = 0x7f050015;
        public static final int ic_plusone_small_off_client = 0x7f050016;
        public static final int ic_plusone_standard_off_client = 0x7f050017;
        public static final int ic_plusone_tall_off_client = 0x7f050018;
    }
    public static final class id {
        private id() {}

        public static final int adjust_height = 0x7f060006;
        public static final int adjust_width = 0x7f060007;
        public static final int auto = 0x7f060009;
        public static final int dark = 0x7f06000e;
        public static final int icon_only = 0x7f060014;
        public static final int item_touch_helper_previous_elevation = 0x7f060017;
        public static final int light = 0x7f060018;
        public static final int none = 0x7f06001c;
        public static final int normal = 0x7f06001d;
        public static final int standard = 0x7f060023;
        public static final int text = 0x7f060025;
        public static final int text2 = 0x7f060026;
        public static final int wide = 0x7f060029;
    }
    public static final class integer {
        private integer() {}

        public static final int google_play_services_version = 0x7f070001;
    }
    public static final class string {
        private string() {}

        public static final int common_google_play_services_enable_button = 0x7f0a000f;
        public static final int common_google_play_services_enable_text = 0x7f0a0010;
        public static final int common_google_play_services_enable_title = 0x7f0a0011;
        public static final int common_google_play_services_install_button = 0x7f0a0012;
        public static final int common_google_play_services_install_text = 0x7f0a0013;
        public static final int common_google_play_services_install_title = 0x7f0a0014;
        public static final int common_google_play_services_notification_ticker = 0x7f0a0016;
        public static final int common_google_play_services_unknown_issue = 0x7f0a0017;
        public static final int common_google_play_services_unsupported_text = 0x7f0a0018;
        public static final int common_google_play_services_update_button = 0x7f0a0019;
        public static final int common_google_play_services_update_text = 0x7f0a001a;
        public static final int common_google_play_services_update_title = 0x7f0a001b;
        public static final int common_google_play_services_updating_text = 0x7f0a001c;
        public static final int common_google_play_services_wear_update_text = 0x7f0a001d;
        public static final int common_open_on_phone = 0x7f0a001e;
        public static final int common_signin_button_text = 0x7f0a001f;
        public static final int common_signin_button_text_long = 0x7f0a0020;
    }
    public static final class style {
        private style() {}

        public static final int Theme_IAPTheme = 0x7f0b000c;
    }
    public static final class styleable {
        private styleable() {}

        public static final int[] AdsAttrs = { 0x7f010000, 0x7f010001, 0x7f010002 };
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
        public static final int[] LoadingImageView = { 0x7f010004, 0x7f01000f, 0x7f010010 };
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] RecyclerView = { 0x10100c4, 0x10100f1, 0x7f010011, 0x7f010012, 0x7f010014, 0x7f010015 };
        public static final int RecyclerView_android_orientation = 0;
        public static final int RecyclerView_android_descendantFocusability = 1;
        public static final int RecyclerView_layoutManager = 2;
        public static final int RecyclerView_reverseLayout = 3;
        public static final int RecyclerView_spanCount = 4;
        public static final int RecyclerView_stackFromEnd = 5;
        public static final int[] SignInButton = { 0x7f010003, 0x7f010005, 0x7f010013 };
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
    }
}
