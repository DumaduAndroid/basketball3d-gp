package com.dumadugames.basketball;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;

import com.dumadu.iab.util.IabHelper;
import com.dumadu.iab.util.IabHelper.OnConsumeFinishedListener;
import com.dumadu.iab.util.IabHelper.OnIabPurchaseFinishedListener;
import com.dumadu.iab.util.IabHelper.OnIabSetupFinishedListener;
import com.dumadu.iab.util.IabResult;
import com.dumadu.iab.util.Purchase;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.GamesActivityResultCodes;
import com.google.android.gms.plus.Plus;
import com.unity3d.player.UnityPlayer;

import java.util.StringTokenizer;

import static com.dumadu.google.playgames.BaseGameUtils.resolveConnectionFailure;
import static com.google.android.gms.games.Games.API;
import static com.google.android.gms.games.Games.Achievements;
import static com.google.android.gms.games.Games.Leaderboards;
import static com.google.android.gms.games.Games.SCOPE_GAMES;
import static com.google.android.gms.plus.Plus.SCOPE_PLUS_LOGIN;

public class Basketball3D extends Activity implements RewardedVideoAdListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    protected UnityPlayer mUnityPlayer; // don't change the name of this variable; referenced from native code
    private FrameLayout layout;

    private static GoogleApiClient gamesClient;

    private InterstitialAd interstitialAd;
    private AdView banner;
    private RewardedVideoAd mRewardedVideoAd;
    Toast toast;
    static String toastmessage = "";
    private static GoogleApiClient mGoogleApiClient;
    private boolean mResolvingConnectionFailure = false;
    private static final int RC_SIGN_IN = 9001;

    private static boolean exit;
    private static boolean RemoveAds;

    private static Handler handler;
    private static final int SHOW_INTRESTITIAL = 1;
    private static final int SHOW_BANNER_BOTTOM = 2;
    private static final int SHOW_BANNER_TOP = 3;
    private static final int HIDE_BANNER = 4;
    private static final int SHOW_EXIT = 5;
    private static final int GOOGLE_LOGIN = 6;
    private static final int TOAST = 7;
    private static final int START_ALPHONSO = 8;
    private static final int STOP_ALPHONSO = 9;
    private static final int SHOW_VIDEO = 10;
    private static final int LOGIN_DG = 11;
    private static String toastMsg;

    private static final int RC_INAPP = 20001;
    private static final String COINS_SKU_10000 = "com.basketball.coins10000";
    private static final String COINS_SKU_20000 = "com.basketball.coins20000";
    private static final String COINS_SKU_40000 = "com.basketball.coins40000";
    private static final String COINS_SKU_70000 = "com.basketball.coins70000";
    private static final String COINS_SKU_100000 = "com.basketball.coins100000";
    private static final String COINS_SKU_200000 = "com.basketball.coins200000";
    private static IabHelper billingHelper;
    private static final String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgYfWbpq81yuapRz7KLpkV5ungsnS8u9Tx+4x6Qfkd8FX+0Y9yKbOFL9v1GUHx1OoMO5cchePkCD1XQFqJ1mbGdcGSPhOtARSABy9SUur5/afEGNta+EQePtb5CabwDHItWDMrJf2rt2FsKAOiIxHRCRXCIQLJY+xIaSgkc4xyygTBH9Ml9FpDqNQvaUZ83k5vRb6hbOqVfQAV/DIkBXUWJlHfiAHU/c39Sd8d0/se3P1AHjb1o8bvTElykv7bMHJP/6NCfHfvz2PaaJpL4d0rl3WXhDoD9Q7N/eRQpnucACGlwcoCG0NVo9ZS/61OwoZXWrrpxdxT8YfW9bKRyJcxQIDAQAB";
    //	private static final String SLIDEME_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuiSUuFFRBgHMnbBoa2NqC75S4xSR6Rx9BKW95GB1PHl8xrs8k3v8iHssSI6HLmsrGj1AIfK27jerWXupKvpTE0Q8XLfCoX1YxhkR3QdkppPqjSD4HH3/nvUlc1hySLWog8epRU3GX+rfHlLhNLD5oSFbOR53itKLLmBCRHeM8QUrIxI6EiNep6+EHXM56jh+nQ11PgDVgTrPkEF8CiFb11GQGzuUaCdAO4cWTGI+c4/HP6qktapmfNK0E+301HvXZGsanABy2DyOpd99csRD1WHoliaSdNW3Gf7kjfHhngtaaoKgcxHgPKZbbwIbV6qvdHhrTHMG/ot5TY0BsyNJ9QIDAQAB";
    private static boolean isBillingSupported;

    private View mDecorView;

    //Alphonso
    private static final String TAG = "Callback";
    private static final String APP_ID = "D-BsktBal3D-A-Nt65u8C3MJjHt36X";
    private ResultReceiver mStatusReceiver = null;
    private ResultReceiver mIdentificationReceiver = null;
    private static Activity activity;
    final Handler handler_alphonso = new Handler();
    static String countryName;
    SharedPreferences sharedpreferences;
    static Editor editor;
    boolean isVideoLoading;

    //Firebase Analytics
//	private static FirebaseAnalytics mFirebaseAnalytics;

    // Setup activity layout
    @SuppressLint({"HandlerLeak", "CommitPrefEdits"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDecorView = getWindow().getDecorView();

        setContentView(R.layout.unity_game_screen);
        layout = (FrameLayout) findViewById(R.id.game_view);

        getWindow().takeSurface(null);
        setTheme(android.R.style.Theme_NoTitleBar_Fullscreen);
        getWindow().setFormat(PixelFormat.RGBX_8888);

        mUnityPlayer = new UnityPlayer(this);
        layout.addView(mUnityPlayer.getView());

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Plus.API).addScope(SCOPE_PLUS_LOGIN)
                .addApi(API).addScope(SCOPE_GAMES)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
        mDecorView = getWindow().getDecorView();

        //Firebase Analytics
//		mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //Alphonso
        activity = this;
        countryName = this.getResources().getConfiguration().locale.getCountry();
        Log.e("test", "COUNTRY: " + countryName);
//						if (checkPermission_RecordAudio()) {
//							setupAlphonsoService();	
//						}else {
//							requestPermission();
//						}

        //Alphonso Mic Status
        sharedpreferences = getSharedPreferences("AlphonsoMicStatus", Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        banner = (AdView) findViewById(R.id.banner);
        banner.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.e("Ads", "Admob banner loaded.");
            }
        });
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.admob_interstitial));
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // TODO Auto-generated method stub
                if (exit) {
                    //	finish();
                    nativeExitPopup();
                    return;
                }
                if (!RemoveAds) {
                    loadInterstitialAd();
                }
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.e("Ads", "Admob Interstitial Loaded");
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                //total ads shown event
                adsanalyticsEvent("IntShown", null);
            }
        });

        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.setRewardedVideoAdListener(this);
        loadRewardedVideoAd();

//		Map<String, String> storeKeys = new HashMap<String, String>();
//		storeKeys.put(IabHelper.NAME_GOOGLE, base64EncodedPublicKey);
//		storeKeys.put("SlideME", SLIDEME_PUBLIC_KEY);

        billingHelper = new IabHelper(this, base64EncodedPublicKey);
        billingHelper.startSetup(new OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                // TODO Auto-generated method stub
                if (!result.isSuccess()) {
                    isBillingSupported = false;
                    return;
                }
                isBillingSupported = true;
//				if (billingHelper.getConnectedAppstoreName().contains("google")) {
//					new UpdateAlertTask().execute(Basketball3D.this);
//				}
            }
        });

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                // TODO Auto-generated method stub
                switch (msg.what) {
                    case SHOW_BANNER_TOP:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                                params.addRule(RelativeLayout.CENTER_HORIZONTAL);
                                params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                                banner.setLayoutParams(params);
                                banner.setVisibility(View.VISIBLE);
                            }
                        });
                        break;
                    case SHOW_BANNER_BOTTOM:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                                params.addRule(RelativeLayout.CENTER_HORIZONTAL);
                                params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                                banner.setLayoutParams(params);
                                banner.setVisibility(View.VISIBLE);
                            }
                        });
                        break;
                    case HIDE_BANNER:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                banner.setVisibility(View.GONE);
                            }
                        });
                        break;
                    case SHOW_INTRESTITIAL:
                        runOnUiThread(new Runnable() {
                            public void run() {
                                // TODO Auto-generated method stub
                                if (!RemoveAds) {
                                    adsanalyticsEvent("IntGameReq", null);
                                    if (interstitialAd.isLoaded()) {
                                        interstitialAd.show();
                                    } else {
                                        loadInterstitialAd();
                                    }
                                }
                            }
                        });
                        break;
                    case GOOGLE_LOGIN:
                        runOnUiThread(new Runnable() {
                            public void run() {
                                // TODO Auto-generated method stub
                                mGoogleApiClient.connect();
                            }
                        });
                        break;
                    case SHOW_EXIT:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                adsanalyticsEvent("IntGameReq", null);
                                if (!exit && interstitialAd.isLoaded() && !RemoveAds) {
                                    interstitialAd.show();
                                    exit = true;
                                } else {
                                    //	finish();
                                    nativeExitPopup();

                                }
                            }
                        });
                        break;
                    case TOAST:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                Toast.makeText(getApplicationContext(), toastMsg, Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    case SHOW_VIDEO:
                        runOnUiThread(new Runnable() {
                            public void run() {
                                if (mRewardedVideoAd.isLoaded()) {
                                    mRewardedVideoAd.show();
                                    Log.e("test", "isLoaded");
                                } else {
                                    toastmessage = "No Videos Available..";
                                    try {
                                        toast.getView().isShown();     // true if visible
                                        toast.setText(toastmessage);
                                    } catch (Exception e) {         // invisible if exception
                                        toast = Toast.makeText(Basketball3D.this, toastmessage, Toast.LENGTH_SHORT);
                                    }
                                    toast.show();
                                    UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "Receiver", "VideoAds_false");
                                    Log.e("test", "Video Ad Failed: is not Loaded");
                                    Log.e("test", "is not Loaded");
                                    loadRewardedVideoAd();
                                }
                            }
                        });
                        break;
                }
            }
        };

        SharedPreferences sharedPreferences = getSharedPreferences("RemoveAds", Context.MODE_PRIVATE);
        RemoveAds = sharedPreferences.getBoolean("isFull", false);

        if (android.os.Build.VERSION.SDK_INT >= 19) {
            UiChangeListener();
        }

    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        mGoogleApiClient.connect();
        if (!RemoveAds) {
            loadBanner();
            loadInterstitialAd();
        }

    }

    // Pause Unity
    @Override
    protected void onPause() {
        super.onPause();
        mUnityPlayer.pause();

    }

    // Resume Unity
    @Override
    protected void onResume() {
        super.onResume();
        mUnityPlayer.resume();

    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
    }

    // Quit Unity
    @Override
    protected void onDestroy() {
        mUnityPlayer.quit();
        super.onDestroy();
    }

    // This ensures the layout will be correct.
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mUnityPlayer.configurationChanged(newConfig);
    }

    // Notify Unity of the focus change.
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        mUnityPlayer.windowFocusChanged(hasFocus);
        if (hasFocus && android.os.Build.VERSION.SDK_INT >= 19) {
            mDecorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    // For some reason the multiple keyevent type is not supported by the ndk.
    // Force event injection by overriding dispatchKeyEvent().
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_MULTIPLE)
            return mUnityPlayer.injectEvent(event);
        return super.dispatchKeyEvent(event);
    }

    // Pass any events not handled by (unfocused) views straight to UnityPlayer
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return mUnityPlayer.injectEvent(event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return mUnityPlayer.injectEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mUnityPlayer.injectEvent(event);
    }

    /*API12*/
    public boolean onGenericMotionEvent(MotionEvent event) {
        return mUnityPlayer.injectEvent(event);
    }

    @Override
    protected void onActivityResult(int request, int response, Intent data) {
        if (!billingHelper.handleActivityResult(request, response, data)) {
            super.onActivityResult(request, response, data);
        }
        if (response == GamesActivityResultCodes.RESULT_RECONNECT_REQUIRED && request == 5001) {
            mGoogleApiClient.disconnect();
        }
        mResolvingConnectionFailure = false;
        if (request == RC_SIGN_IN && response == RESULT_OK) {
            mGoogleApiClient.connect();
        }

    }

    // to show Toast message from UI thread.
    public static void showToastMsg(String msg, Activity activity) {
        toastMsg = msg;
        Message message = new Message();
        message.what = TOAST;
        handler.handleMessage(message);
    }

    public static void submitScore(String leaderboardId, int score, Activity activity) {
        if (mGoogleApiClient != null) {
            Leaderboards.submitScore(mGoogleApiClient, leaderboardId, score);
        } else {
            SharedPreferences preferences = activity.getSharedPreferences("GameServices", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt(leaderboardId, Math.max(score, preferences.getInt(leaderboardId, 0)));
            editor.commit();

            Message message = new Message();
            message.what = LOGIN_DG;
            handler.handleMessage(message);
        }
    }

    public static void unlockAchievement(String id, Activity activity) {
        if (mGoogleApiClient != null) {
            Achievements.unlock(mGoogleApiClient, id);
        } else {
            SharedPreferences preferences = activity.getSharedPreferences("GameServices", Context.MODE_PRIVATE);
            Editor editor = preferences.edit();
            editor.putBoolean(id, true);
            editor.commit();
        }
    }

    public static void showLeaderBoards(final Activity activity) {
        if (mGoogleApiClient != null) {
//            activity.startActivityForResult(Games.Leaderboards.getLeaderboardIntent(mGoogleApiClient, activity.getString(R.string.leaderboard_high_score)), 5001);
            activity.startActivityForResult(Leaderboards.getAllLeaderboardsIntent(mGoogleApiClient), 5001);
        } else {
            Message message = new Message();
            message.what = GOOGLE_LOGIN;
            handler.handleMessage(message);
        }
    }

    public static void showAchievements(Activity activity) {
        if (mGoogleApiClient != null) {
            activity.startActivityForResult(Achievements.getAchievementsIntent(mGoogleApiClient), 5001);
        } else {
            Message message = new Message();
            message.what = GOOGLE_LOGIN;
            handler.handleMessage(message);
        }
    }

    private void updateGameServices(SharedPreferences preferences) {
        // TODO Auto-generated method stub
        Leaderboards.submitScore(mGoogleApiClient, getString(R.string.leaderboard_time_attack), preferences.getInt(getString(R.string.leaderboard_time_attack), 0));
        Leaderboards.submitScore(mGoogleApiClient, getString(R.string.leaderboard_sudden_death), preferences.getInt(getString(R.string.leaderboard_sudden_death), 0));
        Leaderboards.submitScore(mGoogleApiClient, getString(R.string.leaderboard_challenge), preferences.getInt(getString(R.string.leaderboard_challenge), 0));

        if (preferences.getBoolean(getString(R.string.achievement_baby_steps), false))
            Achievements.unlock(mGoogleApiClient, getString(R.string.achievement_baby_steps));
        if (preferences.getBoolean(getString(R.string.achievement_living_on_a_string), false))
            Achievements.unlock(mGoogleApiClient, getString(R.string.achievement_living_on_a_string));
        if (preferences.getBoolean(getString(R.string.achievement_challenge_demolished), false))
            Achievements.unlock(mGoogleApiClient, getString(R.string.achievement_challenge_demolished));
        if (preferences.getBoolean(getString(R.string.achievement_thank_you), false))
            Achievements.unlock(mGoogleApiClient, getString(R.string.achievement_thank_you));
        if (preferences.getBoolean(getString(R.string.achievement_more_and_more_games), false))
            Achievements.unlock(mGoogleApiClient, getString(R.string.achievement_more_and_more_games));
        if (preferences.getBoolean(getString(R.string.achievement_shopping_spree), false))
            Achievements.unlock(mGoogleApiClient, getString(R.string.achievement_shopping_spree));
        if (preferences.getBoolean(getString(R.string.achievement_first_buy), false))
            Achievements.unlock(mGoogleApiClient, getString(R.string.achievement_first_buy));
        if (preferences.getBoolean(getString(R.string.achievement_bonus_spree), false))
            Achievements.unlock(mGoogleApiClient, getString(R.string.achievement_bonus_spree));
        if (preferences.getBoolean(getString(R.string.achievement_score_freak), false))
            Achievements.unlock(mGoogleApiClient, getString(R.string.achievement_score_freak));
    }

    private static OnIabPurchaseFinishedListener purchaseFinishedListener = new OnIabPurchaseFinishedListener() {
        @Override
        public void onIabPurchaseFinished(IabResult result, Purchase info) {
            // TODO Auto-generated method stub
            if (result.isFailure()) {
                UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "Receiver", "false");
                return;
            }
            if (info.getSku().equals(COINS_SKU_10000)
                    || info.getSku().equals(COINS_SKU_20000)
                    || info.getSku().equals(COINS_SKU_40000)
                    || info.getSku().equals(COINS_SKU_70000)
                    || info.getSku().equals(COINS_SKU_100000)
                    || info.getSku().equals(COINS_SKU_200000)) {
                billingHelper.consumeAsync(info, consumeFinishedListener);
            }
        }
    };

    private static OnConsumeFinishedListener consumeFinishedListener = new OnConsumeFinishedListener() {
        @SuppressLint("ApplySharedPref")
        @Override
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            // TODO Auto-generated method stub
            if (result.isSuccess()) {
                Editor editor = activity.getSharedPreferences("RemoveAds", Context.MODE_PRIVATE).edit();
                editor.putBoolean("isFull", true);
                editor.commit();
                RemoveAds = true;
                hideBanner();
                UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "Receiver", "true");
            } else {
                UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "Receiver", "false");
            }
        }
    };

    public void UiChangeListener() {
        final View decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                }
            }
        });
    }

    // Callback methods from Unity
    public static void purchaseItem(String itemId, Activity activity) {
        if (isBillingSupported) {
            billingHelper.launchPurchaseFlow(activity, itemId, RC_INAPP, purchaseFinishedListener, null);
        } else {
            toastMsg = "Inapp Billing is not supported";
            Message message = new Message();
            message.what = TOAST;
            handler.handleMessage(message);
        }
    }

    public static void adsanalyticsEvent(String message, Activity activity) {
        Log.e("test", "Firebase Analytics: " + message);
//		StringTokenizer tokenizer = new StringTokenizer(message, "_");
//		tracker.send(new HitBuilders.EventBuilder().setCategory(tokenizer.nextToken()).setAction(tokenizer.nextToken()).setLabel(tokenizer.nextToken()).build());
//		String eventName = tokenizer.nextToken();//Category
//		String action = tokenizer.nextToken();//Action
//		String label = tokenizer.nextToken();//Label

//		Bundle bundle = new Bundle();
//		bundle.putString("Action", "");
//		bundle.putString("Label", "");
//		mFirebaseAnalytics.logEvent(message, null);
    }

    public static void gameanalyticsEvent(String message, Activity activity) {
        Log.e("test", "Firebase Analytics: " + message);
        StringTokenizer tokenizer = new StringTokenizer(message, "_");
//		tracker.send(new HitBuilders.EventBuilder().setCategory(tokenizer.nextToken()).setAction(tokenizer.nextToken()).setLabel(tokenizer.nextToken()).build());
        String category = tokenizer.nextToken();//Category
        String action = tokenizer.nextToken();//Action
        String label = tokenizer.nextToken();//Label

        Bundle bundle = new Bundle();
//		bundle.putString("Action", action);
        bundle.putString("Label", label);
//		mFirebaseAnalytics.logEvent(action, bundle);
    }

    public static void showIntrestitial(Activity activity) {
        if (RemoveAds) {
            return;
        }
        Message message = new Message();
        message.what = SHOW_INTRESTITIAL;
        handler.handleMessage(message);
    }

    public static void showBanner(final boolean top, Activity activity) {
        if (RemoveAds) {
            return;
        }
        if (top) {
            Message message = new Message();
            message.what = SHOW_BANNER_TOP;
            handler.handleMessage(message);
        } else {
            Message message = new Message();
            message.what = SHOW_BANNER_BOTTOM;
            handler.handleMessage(message);
        }
    }

    public static void hideBanner(final boolean top, Activity activity) {
        hideBanner();
    }

    public static void hideBanner() {
        Message message = new Message();
        message.what = HIDE_BANNER;
        handler.handleMessage(message);
    }

    public static void showExitPopup(Activity activity) {
        if (RemoveAds) {
            activity.finish();
            return;
        }
        Message message = new Message();
        message.what = SHOW_EXIT;
        handler.handleMessage(message);
    }

    public static void analyticsEvent(String message, Activity activity) {
        StringTokenizer tokenizer = new StringTokenizer(message, "_");
    }

    public static void analyticsScreen(String message, Activity activity) {
    }

    public static void moregames(Activity activity) {
        activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pub:Dumadu Games")));
    }

    public static void showVideoAd(Activity activity) {
        analyticsEvent("Rewarded_Total RewardedVideo requests from game_Total RewardedVideo requests from game", null);
        Message message = new Message();
        message.what = SHOW_VIDEO;
        handler.handleMessage(message);
    }

    private void loadRewardedVideoAd() {
        if (!mRewardedVideoAd.isLoaded() && !isVideoLoading) {
            isVideoLoading = true;
            Bundle extras = new Bundle();
            extras.putBoolean("_noRefresh", true);
            AdRequest adRequest = new AdRequest.Builder()
                    .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                    .build();
            mRewardedVideoAd.loadAd(getString(R.string.admob_rewarded_video), adRequest);
        }
    }

    private void loadInterstitialAd() {
        if (!interstitialAd.isLoading() || !interstitialAd.isLoaded()) {
            if (isNetworkAvailable()) {
                interstitialAd.loadAd(new AdRequest.Builder().build());
            }
        }
    }

    private void loadBanner() {
        if (!banner.isLoading()) {
            if (isNetworkAvailable()) {
                banner.loadAd(new AdRequest.Builder().build());
            }
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public void nativeExitPopup() {
        AlertDialog.Builder alertDialogBuilder;
        if (android.os.Build.VERSION.SDK_INT >= 11) {
            alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
        } else {
            alertDialogBuilder = new AlertDialog.Builder(this);
        }
        alertDialogBuilder.setTitle(getString(R.string.app_name));
        alertDialogBuilder
                .setMessage("Do you want to exit ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Basketball3D.this.finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        exit = false;
                        if (!RemoveAds) {
                            loadInterstitialAd();
                        }
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public static void requestPermissionExternalStorage() {
        if (!checkPermissionExternalStorage()) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

    //Checking permission
    private static boolean checkPermissionExternalStorage() {
        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        Log.e("Ads", "Admob Video Ad loaded.");
        isVideoLoading = false;
    }

    @Override
    public void onRewardedVideoAdOpened() {

    }

    @Override
    public void onRewardedVideoStarted() {
        analyticsEvent("Rewarded_RewardedVideo started_RewardedVideo started", null);
    }

    @Override
    public void onRewardedVideoAdClosed() {

    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        analyticsEvent("Rewarded_RewardedVideo closed_RewardedVideo closed", null);
        UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "Receiver", "true");
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {

    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        isVideoLoading = false;
        loadRewardedVideoAd();

    }

    @Override
    public void onRewardedVideoCompleted() {

    }

    public void onConnected(Bundle bundle) {
        Log.e(TAG, "onConnected() called with: bundle = [" + bundle + "]");
        Toast.makeText(this, "Login Success..", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
        Log.e(TAG, "onConnectionSuspended() called with: i = [" + i + "]");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed() called with: connectionResult = [" + connectionResult + "]" + connectionResult.getErrorCode());
        if (mResolvingConnectionFailure) {
            Log.d(TAG, "onConnectionFailed() ignoring connection failure; already resolving.");
            return;
        }

        mResolvingConnectionFailure = resolveConnectionFailure(this, mGoogleApiClient,
                connectionResult, RC_SIGN_IN, 0);
    }
}
