package com.dumadugames.unityandroid;

import android.app.Activity;
import android.util.Log;

import com.dumadugames.basketball.Basketball3D;
import com.dumadugames.basketball.R;
import com.unity3d.player.UnityPlayer;

import java.util.StringTokenizer;

public class JarClass {
	
	public static void purchaseItem(boolean managed, int id, Activity activity) {
		String itemId = "No Id";
	    switch (id) {
	    case 1:
	      itemId = "com.basketball.coins10000";
	      break;
	    case 2:
	      itemId = "com.basketball.coins20000";
	      break;
	    case 3:
	      itemId = "com.basketball.coins40000";
	      break;
	    case 4:
	      itemId = "com.basketball.coins70000";
	      break;
	    case 5:
	      itemId = "com.basketball.coins100000";
	      break;
	    case 6:
	      itemId = "com.basketball.coins200000";
	    }

	    Basketball3D.purchaseItem(itemId, activity);
	    Log.e("Callback", "purchaseItem " + itemId);
	}
	
	public static void showIntrestitial(Activity activity) {
		Log.e("Callback", "showIntrestitial");
	//	Basketball3D.showIntrestitial(activity);
	}
	
	public static void showIntrestitial(int label, Activity activity) {
		Basketball3D.showIntrestitial(activity);
		Log.e("Callback", "lable "+label+"Full Screen Ad Displayed");
	}
	
	public static void showBanner(boolean top, Activity activity) {
		Log.e("Callback", "Show Banner "+top);
		Basketball3D.showBanner(top, activity);
	}
	
	public static void hideBanner(boolean top, Activity activity) {
		Basketball3D.hideBanner(top, activity);
	}
	
	public static void submitScore(int id, int score, Activity activity) {
		String leaderboardId = "No Id";
	    switch (id) {
	    case 1:
	      leaderboardId = activity.getString(R.string.leaderboard_time_attack);
	      break;
	    case 2:
	      leaderboardId = activity.getString(R.string.leaderboard_sudden_death);
	      break;
	    case 3:
	      leaderboardId = activity.getString(R.string.leaderboard_challenge);
	      break;
	    }

	    Basketball3D.submitScore(leaderboardId, score, activity);
	    Log.e("Callback", "submitScore " + id + leaderboardId + " " + score);
	}
	
	public static void submitScore(int id, float score, Activity activity) {
		Log.e("Callback", score + " Posted to Leaderboard :float score" + id);
	}
	
	public static void shareScore(int id, int score, Activity activity) {
		Log.e("Callback", score + " score shared " + id);
	}
	
	public static void shareScore(int id, float score, Activity activity) {
		Log.e("Callback", score + " score shared " + id);
	}
	
	public static void unlockAchievement(int id, Activity activity) {
	    String achId = "No Id";
	    if (id == 1)
	      achId = activity.getString(R.string.achievement_baby_steps);
	    else if (id == 2)
	      achId = activity.getString(R.string.achievement_living_on_a_string);
	    else if (id == 3)
	        achId = activity.getString(R.string.achievement_challenge_demolished);
	    else if (id == 4)
	        achId = activity.getString(R.string.achievement_thank_you);
	    else if (id == 5)
	        achId = activity.getString(R.string.achievement_more_and_more_games);
	    else if (id == 6)
	        achId = activity.getString(R.string.achievement_shopping_spree);
	    else if (id == 7)
	        achId = activity.getString(R.string.achievement_first_buy);
	    else if (id == 8)
	        achId = activity.getString(R.string.achievement_bonus_spree);
	    else if (id == 9)
	        achId = activity.getString(R.string.achievement_score_freak);
	    Log.e("Callback", "unlockAchievement " + id + achId);
	    Basketball3D.unlockAchievement(achId, activity);
	}
	
	public static void unlockAchievement(String id, Activity activity) {
		Log.e("Callback", id + "Achievement Unlocked string");
	}
	
	public static void incrementAchievement(String id, int steps, Activity activity) {
		Log.e("Callback", "Achivement Incremented by " + steps);
	}
	
	public static void incrementAchievement(int id, int steps, Activity activity) {
		Log.e("Callback", "Achivement Incremented by " + steps);
	}
	
	public static void showLeaderBoards(Activity activity) {
		Basketball3D.showLeaderBoards(activity);
	}
	
	public static void showAchievements(Activity activity) {
		Basketball3D.showAchievements(activity);
	}
	
	public static void showExitPopUp(Activity activity) {
		Basketball3D.showExitPopup(activity);
	}
	
	public static void showPopUp(String titile, String msg, Activity activity) {
		Log.e("Callback", "PopUp Displayed with " + titile + msg);
	}
	
	public static void submitFlurryEvent(String key, Activity activity) {
		Log.e("Callback", "FlurryEvent : " + key);
	}
	
	public static void like(Activity activity) {
		Log.e("Callback", "facebook");
	}
	
	public static void follow(Activity activity) {
		Log.e("Callback", "twitter");
	}
	
	public static void moreGames(Activity activity) {
		Basketball3D.moregames(activity);
	}
	
	public static void rateApp(Activity activity) {
		Log.e("Callback", "Rate Application");
	}
	
	public static void showGiftiz(Activity activity) {
		Log.e("Callback", "showGiftiz");
	}
	
	public static void giftizMissionDone(Activity activity) {
		Log.e("Callback", "giftizMissionDone");
	}
	
	public static void showVideoAds(Activity activity) {
		Basketball3D.showVideoAd(activity);
	//	UnityPlayer.UnitySendMessage("ExternalInterfaceHandler","Receiver","true");
	    Log.e("Callback", "showVideoAds");
	}
	
	public static void showVideoAds(int id, Activity activity) {
		Basketball3D.showVideoAd(activity);
	//	UnityPlayer.UnitySendMessage("ExternalInterfaceHandler","Receiver","true");
	    Log.e("Callback", "showVideoAds: id: "+id);
	}
	
	public static void shareScreen(Activity activity) {
	}
	
	public static void shareScreen(String path, Activity activity) {
	    Log.e("Callback", "shareScreen Path:"+path);
	}
	
	public static void localNotification(String date, String msg, int id, Activity activity) {
		StringTokenizer tokenizer = new StringTokenizer(date, "_");
	    Log.e("Callback ", id + " notify after " + tokenizer.nextToken() + " " +
	      tokenizer.nextToken() + " " + 
	      tokenizer.nextToken() + " " + 
	      tokenizer.nextToken() + " " + 
	      tokenizer.nextToken() + " " + 
	      tokenizer.nextToken() + "   " + msg);
	}
	
	public static void cancellocalNotification(boolean notification, Activity activity) {
		Log.e("Callback", "cancellocalNotification  " + notification);
	}
	
	public static void analyticsEvent(String message, Activity activity) {
		Basketball3D.analyticsEvent(message, activity);
	}
	
	public static void analyticsScreen(String message, Activity activity) {
		Basketball3D.analyticsScreen(message, activity);
	}
	
	public static void initialize(Activity activity) {
		UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "SetExternalPreferences", "3");
//		Basketball3D.initialize(activity);
	}
	
	public static void gamePlayStart(boolean b, Activity activity) {
		
	}
	
	public static void gamePlayEnd(boolean b, Activity activity) {
		
	}
	
	//For Alphonso
	public static void micStatus(boolean status, Activity activity) {
		Log.e("Callback", "Alphonso micStatus: "+status);
//				UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "Receiver", "MicStatus_true");//MicStatus_false	
	}
	
}